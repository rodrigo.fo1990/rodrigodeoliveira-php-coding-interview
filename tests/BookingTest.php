<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use Src\controllers\Booking;

class BookingTest extends TestCase {

	private $booking;

	/**
	 * Setting default data
	 * @throws \Exception
	 */
	public function setUp(): void {
		parent::setUp();
		$this->booking = new Booking();
	}

	/** @test */
	public function getBookings() {
		$results = $this->booking->getBookings();

		$this->assertIsArray($results);
		$this->assertIsNotObject($results);

		$this->assertEquals($results[0]['id'], 1);
		$this->assertEquals($results[0]['clientid'], 1);
		$this->assertEquals($results[0]['price'], 200);
		$this->assertEquals($results[0]['checkindate'], '2021-08-04 15:00:00');
		$this->assertEquals($results[0]['checkoutdate'], '2021-08-11 15:00:00');
	}

	/** @test */
	public function createBooking(){
		$data = [
			'clientid' => 3,
			'price'=> 120,
			'checkindate' => '2023-06-31 12:00:00',
			'checkoutdate' => '2023-07-05 12:00:00'
		];

		$created_booking = $this->booking->createBooking($data);
		
		$bookings = $this->booking->getBookings();

		$this->assertIsArray($bookings);

		$last_index = count($bookings)-1;

		$this->assertEquals($bookings[$last_index]['id'], $created_booking['id']);
		$this->assertEquals($bookings[$last_index]['clientid'], 3);
		$this->assertEquals($bookings[$last_index]['price'], 120);
		$this->assertEquals($bookings[$last_index]['checkindate'], '2023-06-31 12:00:00');
		$this->assertEquals($bookings[$last_index]['checkoutdate'], '2023-07-05 12:00:00');

	}

	/** @test */
	public function createBookingWithDiscount(){
		$data = [
			'clientid' => 2,
			'price'=> 200,
			'checkindate' => '2023-06-31 12:00:00',
			'checkoutdate' => '2023-07-05 12:00:00'
		];

		$created_booking = $this->booking->createBooking($data);

		$this->assertIsArray($created_booking);
		$this->assertEquals($created_booking['price'], 180);
		
	}
}