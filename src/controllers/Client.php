<?php

namespace Src\controllers;

use Src\helpers\Helpers;
use Src\models\ClientModel;

class Client {

	private function getClientModel(): ClientModel {
		return new ClientModel();
	}
	
	public function getClients() {
		return $this->getClientModel()->getClients();
	}

	public function createClient($client) {
		return $this->getClientModel()->createClient($client);
	}

	public function updateClient($client) {
		return $this->getClientModel()->updateClient($client);
	}

	public function getClientById($id) {
		return $this->getClientModel()->getClientById($id);
	}

	public function getClientsAverageDogAge($id){
		$dogInst = new Dog();
		$clientsDogs = $dogInst->getDogsByClientId($id);

		if (count($clientsDogs) == 0) {
			return 0;
		}

		$ageSum = 0;
		foreach($clientsDogs as $dog){
			$ageSum += $dog['age'];
		}
		
		return $ageSum / count($clientsDogs);
	}
}