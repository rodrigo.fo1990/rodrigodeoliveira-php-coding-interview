<?php

namespace Src\controllers;

use Src\models\DogModel;

class Dog {

	private function getDogModel(): DogModel {
		return new DogModel();
	}

	public function getDogs() {
		return $this->getDogModel()->getDogs();
	}

	public function getDogsByClientId($clientid) {
		$dogs = $this->getDogModel()->getDogs();
		$data = [];
		foreach ($dogs as $dog) {
			if ($dog['clientid'] == $clientid){
				$data[] = $dog;
			}
		}
		return $data;
	}
	
}