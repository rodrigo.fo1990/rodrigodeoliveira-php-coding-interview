<?php

namespace Src\controllers;

use Src\models\BookingModel;

class Booking {

	private function getBookingModel(): BookingModel {
		return new BookingModel();
	}
	
	public function getBookings() {
		return $this->getBookingModel()->getBookings();
	}

	public function createBooking($booking) {
		$clientInst = new Client();

		$avgDogAge = $clientInst->getClientsAverageDogAge($booking['clientid']);
		
		if ($avgDogAge < 10) {
			$booking['price'] = $booking['price'] * 0.9;
		}

		return $this->getBookingModel()->createBooking($booking);
	}
}